#!/usr/bin/env python
#########################################################################
# Advanced Weapons Repair by Faustle (2017)
# Weapon sections parser for generate awr_dynamic_objects.ltx
#########################################################################

import os
import re
import fnmatch

pr_out = []
out = []
dic = {}
lists = []
res = []
broken = []

# Исключения
pat_1 = re.compile(r'\[wpn_.*(?:(hud)|(1p29)|(kobra)|(eot)|(ac10632)|(ps01)|(pso1m21)|(acog))\]')
pattern = '*.ltx'

# Путь к файлам оружия wpn_xxx.ltx,из которых будет извлечена секция и имя файла визуала.
# Допустимо размещение в подкаталогах. Пути указывать с двумя слешами.
root = 'C:\\Games\\...'
# Путь к файлу вывода
out_ltx = "C:\\Games\\...\\awr_dynamic_objects.ltx"

for folder, subdirs, files in os.walk(root):
    for filename in fnmatch.filter(files, pattern):
        fullname = os.path.join(folder, filename)

        with open(fullname) as f:
            lists.append(f.read())

for i in lists:
    out.extend(re.findall(r'^\[wpn_.*?(?=\[wpn|\Z)', i, re.DOTALL | re.MULTILINE))

for n, item in enumerate(out):
    if not re.search(pat_1, item):
        res.append(item)

for i in res:
    dic[str(re.findall(r'(?<=\[)wpn.*?(?=\])', i)[0])] = ''.join(re.findall(r'.*(wpn.*?\.ogf)', i))

for key, val in dic.items():
    if val == "":
        broken.append(key)
        for k in dic.keys():
            if k == (str.rpartition(key, "_")[0]):
                dic[key] = dic[k]
            # if re.search(str(k), str(key)):
            #    dic[key] = dic[k]

print("{} моделей найдено в {} секциях.\n".format(len(dic.values()) - len(broken), len(dic.keys())))

if len(broken) > 0:
    print("Секции оружия с отсутствующими путями или неверными визуалами:")
    for i in broken:
        print(i)
    print("\nВнимание! Пути к визуалам для данных секций не были распознаны и будут автоматически заменены на секцию "
          "по-умолчанию для данного вида. Если этот способ не дал нужного результата, закомментируйте блок "'if'" "
          "на строках 48-49 и раскомментируйте блок на строках 50-51.")

for key, val in dic.items():
    pr_out.append("[awr_m_{}]:awr_physic_object".format(key))
    pr_out.append("visual\t\t\t\t\t\t\t= dynamics\\workshop_room\\awr_weapons\\{}".format(val))
    pr_out.append("script_binding\t\t\t\t\t= bind_physic_object.init")
    pr_out.append("fixed_bones\t\t\t\t\t\t= link")
    pr_out.append("custom_data\t\t\t\t\t\t= scripts\\awr_tiski.ltx\n")

with open(out_ltx, "w") as file:
    print(*pr_out, file=file, sep='\n')
